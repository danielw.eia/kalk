﻿using System;

namespace GitTest
{
    class FunkcjeMat
    {
        public void Dodawanie()
        {
            double a, b, c;
            Console.WriteLine("Obliczanie wartości A+B.");
            Console.Write("Podaj A: ");
            a = Convert.ToDouble(Console.ReadLine());
            Console.Write("Podaj B: ");
            b = Convert.ToDouble(Console.ReadLine());

            c = a + b;
            Console.WriteLine("A+B={0}", c);
        }
    }
    class Program
    {

        static void ListaOperacji()
        {
            Console.WriteLine("LISTA OPERACJI");
            Console.WriteLine("0\t- wyjście z programu");
            Console.WriteLine("1\t- dodawanie liczb: A+B");
        }

        static void Main(string[] args)
        {
            FunkcjeMat mat = new FunkcjeMat();
            int nacisnieto;

            ListaOperacji();

            while (true)
            {
                Console.Write("Podaj numer operacji: ");

                //odczyt liczby z klawiatury i konwersja na liczbę całkowitą
                nacisnieto = Convert.ToInt32(Console.ReadLine());

                //jeśli wybrano 0, kończymy pętlę/program
                if (nacisnieto == 0)
                {
                    break;
                }

                //w zależności od wybranej operacji wywołujemy odpowiednią funkcję
                switch (nacisnieto)
                {
                    case 1: //dodawanie
                        mat.Dodawanie();
                        break;

                    default:
                        Console.WriteLine("Brak operacji o podanym numerze.");
                        ListaOperacji();
                        break;
                }

                
            }
            


        }
    }
}
